//
//  GameScene.swift
//  p05
//
//  Created by Ahmet on 3/22/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit
var score = 0

class GameScene: SKScene {
    
    
    let score_label = SKLabelNode(fontNamed: "Betty's Confetti")
    // sounds
    let correct_sound = SKAction.playSoundFileNamed("correct.wav", waitForCompletion: false)
    let game_over_sound = SKAction.playSoundFileNamed("game_over.wav", waitForCompletion: false)
    let background_song = SKAudioNode(fileNamed: "loop.mp3")
    
    // disco balls
    let image_names = ["blue_disco_ball",
                       "green_disco_ball",
                       "purple_disco_ball",
                       "red_disco_ball",
                       "yellow_disco_ball"]
    
    // game area
    let game_area: CGRect
    
    override init(size: CGSize){
        // universal ratio
        let max_ratio: CGFloat = 16.0/9.0
        let play_width = size.height / max_ratio
        let game_margin = (size.width - play_width) / 2
        game_area = CGRect(x: game_margin, y: 0, width: play_width, height: size.height)
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        score = 0
        
        let background = SKSpriteNode(imageNamed: "background.jpg")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = 0
        self.addChild(background)
        // zoom in effect
        let zoomInAction = SKAction.scale(to: 3, duration: 90)
        background.run(zoomInAction)
        
        add_disc()
        
        score_label.fontSize = 400
        score_label.text = "0"
        score_label.fontColor = .white
        score_label.zPosition = 1
        score_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.85)
        self.addChild(score_label)
        self.addChild(background_song)
        
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let tapped_nodes = nodes(at: location)
            
            for node in tapped_nodes{
                let name_tapped_node = node.name
                if name_tapped_node == "disco_ball"{
                    
                    // deactive node to potential tap agains which will add new disco balls
                    node.name = ""
                    
                    // stop shrinking
                    node.removeAllActions()
                    
                    // remove from parent with fade out animation
                    node.run(SKAction.sequence([
                        SKAction.fadeOut(withDuration: 0.1),
                        SKAction.removeFromParent()
                    ]))
                    
                    self.run(correct_sound)
                    
                    add_disc()
                    score += 1
                    score_label.text = "\(score)"
                    
                    if score % 10 == 0{
                        add_disc()
                    }
                }
            }
        }
    }
    
    // get random
    func random(min:CGFloat, max: CGFloat) -> CGFloat{
        let rand = CGFloat(Float(arc4random()) / 0xFFFFFFFF)
        return rand * (max-min) + min
    }
    func add_disc(){
        
        let random_number = Int(arc4random() % 5)
        
        let disco_ball = SKSpriteNode(imageNamed: "\(image_names[random_number])")
        disco_ball.size.width = 350
        disco_ball.size.height = 250
        disco_ball.name = "disco_ball"
        disco_ball.zPosition = 2
        // set random x and y for next disco ball
        let rand_x = random(min: game_area.minX + disco_ball.size.width / 2,
                            max: game_area.maxX - disco_ball.size.width / 2)
        let rand_y = random(min: game_area.minY + disco_ball.size.height / 2,
                            max: game_area.maxY - disco_ball.size.height / 2)
        disco_ball.position = CGPoint(x: rand_x, y: rand_y)
        self.addChild(disco_ball)
        
        // shrink disco balls
        disco_ball.run(SKAction.sequence([
                SKAction.scale(by: 0, duration: 6),
                game_over_sound,
                SKAction.run({
                    self.game_over_reached()
                })
            ]))
    }
    func game_over_reached(){
        let game_over_scene = GameOverScene(size: self.size)
        game_over_scene.scaleMode = self.scaleMode
        
        let scene_transition = SKTransition.crossFade(withDuration: 0.2)
        self.view!.presentScene(game_over_scene, transition: scene_transition)
        
    }
}
